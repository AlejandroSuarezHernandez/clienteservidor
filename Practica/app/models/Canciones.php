<?php


    namespace models;


    class Canciones
    {
        public $idCancion;
        public $duracion;
        public $ano;
        public $nombre;
        public function __construct($idCancion2,$duracion2,$ano2,$nombre2)
        {
            $this->idCancion=$idCancion2;
            $this->duracion=$duracion2;
            $this->ano=$ano2;
            $this->nombre=$nombre2;
        }
        function setIdCancion($idCancion2){
            $this->idCancion=$idCancion2;
        }
        function getIdCancion(){
            return $this->idCancion;
        }
        function setDuracion($duracion2){
            $this->duracion=$duracion2;
        }
        function getDuracion(){
            return $this->duracion;
        }
        function setAno($ano2){
            $this->ano=$ano2;
        }
        function getAnon(){
            return $this->ano;
        }
        function setNombre($nombre2){
            $this->nombre=$nombre2;
        }
        function getNombre(){
            return $this->nombre;
        }
    }
?>