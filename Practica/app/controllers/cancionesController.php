<?php
include "app/models/Canciones.php";
use models\Canciones;
    class cancionesController{
        var $canciones;
        public function __construct()
        {
            $this->canciones=[
                1 => new Canciones("1","5","2000","Rola 1"),
                2 => new Canciones("2","6","2001","Rola 2"),
                3 => new Canciones("3","3","2002","Rola 3"),
                4 => new Canciones("4","2","2003","Rola 4"),
                5 => new Canciones("5","4","2004","Rola 5"),
                6 => new Canciones("6","9","2005","Rola 6"),
                7 => new Canciones("7","2","2006","Rola 7"),
                8 => new Canciones("8","9","2007","Rola 8"),
                9 => new Canciones("9","2","2008","Rola 9"),
                10 => new Canciones("10","1","1999","Rola 10"),
            ];

        }
        public function vista(){
            $cancionero = $this->canciones;
            $needle=$_POST["bus"];
            require ("app/views/Busqueda.php");
        }
        public function bus(){
            require ("app/views/Index.php");
        }
    }
?>