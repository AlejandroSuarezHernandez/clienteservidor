<?php


namespace models;


class Conexion
{
    private $host ="localhost";
    private $usuario="root";
    private $contrasenia="";
    private $base="";
    public $con;
    public function __construct()
    {
        $enlace = mysqli_connect($this->host, $this->usuario, $this->contrasenia, $this->base);

        if (!$enlace) {

            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }
}